<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DataDiri;

class DataDiriController extends Controller
{
   public function index()
   {
    $dataDiris = DataDiri::all();
    return view('data_diri.index',['profiles'=> $dataDiris]);
   }
}
